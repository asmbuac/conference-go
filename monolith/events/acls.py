import requests, json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}&{state}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    content = json.loads(r.content)
    try:
        return {"photo_url": content["photos"][0]["src"]["original"]}
    except:
        return {"photo_url": None}


def get_weather(city, state):
    location_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    location_r = requests.get(location_url)
    location = json.loads(location_r.content)
    lat = location[0]["lat"]
    lon = location[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    weather_r = requests.get(weather_url)
    weather_d = json.loads(weather_r.content)
    try:
        return {
            "temperature": str("%.0f" % (weather_d["main"]["temp"])) + "°F",
            "description": weather_d["weather"][0]["description"],
        }
    except:
        return None
